FROM registry.sicredi.in/sicredi-openjdk11:latest
EXPOSE 8080
ADD /build/libs/accounting-factory-report*.jar /opt/api.jar
ENTRYPOINT exec java $JAVA_OPTS -jar /opt/api.jar