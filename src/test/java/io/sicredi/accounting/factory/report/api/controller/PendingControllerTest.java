//package io.sicredi.accounting.factory.report.api.controller;
//
//import io.sicredi.accounting.factory.report.business.pending.service.PendingService;
//import io.sicredi.accounting.factory.report.infrastructure.exception.BusinessException;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.context.ApplicationContext;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.reactive.server.WebTestClient;
//import reactor.core.publisher.Mono;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class PendingControllerTest {
//
//    private static final String URI = "/pending";
//    private static final String FILE_NAME = "marcos-teste.csv";
//    private static final String USER = "marcos";
//    private static final String PROJECTION = "header1;header2;header3";
//    private static final String FILTER = "field1:10,20,30;field2:50";
//    private static final String STATUS = "OPEN";
//
//    @Autowired
//    private ApplicationContext context;
//
//    @MockBean
//    private PendingService service;
//
//    private WebTestClient webTestClient;
//
//    @BeforeEach
//    void setup() {
//        this.webTestClient = WebTestClient.bindToApplicationContext(this.context).build();
//    }
//
//    @Test
//    void makeReportTest_no_query_param() {
//        webTestClient.get()
//                .uri(builder -> builder.path(URI).build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isBadRequest()
//                .expectBody()
//                .jsonPath("$.timestamp").isNotEmpty()
//                .jsonPath("$.path").isNotEmpty()
//                .jsonPath("$.error").isNotEmpty()
//                .jsonPath("$.message").isNotEmpty();
//    }
//
//    @Test
//    void makeReportTest_user_empty() {
//        webTestClient.get()
//                .uri(builder -> builder.path(URI)
//                        .queryParam("user", "      ")
//                        .build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isBadRequest()
//                .expectBody()
//                .jsonPath("$.timestamp").isNotEmpty()
//                .jsonPath("$.path").isNotEmpty()
//                .jsonPath("$.error").isNotEmpty()
//                .jsonPath("$.message").isNotEmpty();
//    }
//
//    @Test
//    void makeReportTest_projection_invalid() {
//        webTestClient.get()
//                .uri(builder -> builder.path(URI)
//                        .queryParam("projection", "adasdnasd.sadsad,sdad")
//                        .build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isBadRequest()
//                .expectBody()
//                .jsonPath("$.timestamp").isNotEmpty()
//                .jsonPath("$.path").isNotEmpty()
//                .jsonPath("$.error").isNotEmpty()
//                .jsonPath("$.message").isNotEmpty();
//    }
//
//    @Test
//    void makeReportTest_status_invalid() {
//        webTestClient.get()
//                .uri(builder -> builder.path(URI)
//                        .queryParam("status", "adasdnasd.sadsad,sdad")
//                        .build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isBadRequest()
//                .expectBody()
//                .jsonPath("$.timestamp").isNotEmpty()
//                .jsonPath("$.path").isNotEmpty()
//                .jsonPath("$.error").isNotEmpty()
//                .jsonPath("$.message").isNotEmpty();
//    }
//
//    @Test
//    void makeReportTest_business_exception() {
//
//        Mockito.when(service.process(Mockito.any())).thenReturn(Mono.error(new BusinessException("Teste de exceção")));
//
//        webTestClient.get()
//                .uri(builder -> builder.path(URI)
//                        .queryParam("user", USER)
//                        .queryParam("projection", PROJECTION)
//                        .queryParam("filter", FILTER)
//                        .queryParam("status", STATUS)
//                        .build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
//                .expectBody()
//                .jsonPath("$.timestamp").isNotEmpty()
//                .jsonPath("$.path").isNotEmpty()
//                .jsonPath("$.error").isNotEmpty()
//                .jsonPath("$.message").isEqualTo("Teste de exceção");
//    }
//
//    @Test
//    void makeReportTest() {
//        Mockito.when(service.process(Mockito.any())).thenReturn(Mono.just(FILE_NAME));
//
//        webTestClient.get()
//                .uri(builder -> builder.path(URI)
//                        .queryParam("user", USER)
//                        .queryParam("projection", PROJECTION)
//                        .queryParam("filter", FILTER)
//                        .queryParam("status", STATUS)
//                        .build())
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isEqualTo(HttpStatus.CREATED)
//                .expectHeader().valueEquals("location", FILE_NAME);
//    }
//
//}
