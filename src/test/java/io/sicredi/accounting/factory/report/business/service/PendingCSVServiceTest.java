//package io.sicredi.accounting.factory.report.business.service;
//
//import io.sicredi.accounting.factory.report.Application;
//import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
//import io.sicredi.accounting.factory.report.infrastructure.helper.FileHelper;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
//@ExtendWith(SpringExtension.class)
//class PendingCSVServiceTest {
//
//    private static final String FILE_NAME = "arquivo_teste.csv";
//
//    private List<String> projection = new ArrayList<>(Arrays.asList("header1", "header2", "header3", "header4"));
//
//    private String bucketName = "pending";
//
//    @Autowired
//    FileHelper fileHelper;
//
//    @Test
//    void createRowTest() {
//        var pending = new Pending();
//        pending.setTransaction(new HashMap<>());
//        pending.getTransaction().put("company", "0116");
//        pending.getTransaction().put("branch", "05");
//        pending.getTransaction().put("account", "1889200159");
//        pending.getTransaction().put("type", "C");
//
//        var result = fileHelper.createRow(projection, pending);
//
//        assertTrue(result.stream().allMatch(a -> a.toString().isBlank()));
//
//        projection = new ArrayList<>(Arrays.asList("company", "branch", "account", "type"));
//
//        result = fileHelper.createRow(projection, pending);
//
//        assertTrue(result.stream().anyMatch(a -> a.toString().equalsIgnoreCase("0116")));
//        assertTrue(result.stream().anyMatch(a -> a.toString().equalsIgnoreCase("05")));
//        assertTrue(result.stream().anyMatch(a -> a.toString().equalsIgnoreCase("1889200159")));
//        assertTrue(result.stream().anyMatch(a -> a.toString().equalsIgnoreCase("C")));
//    }
//
//}
