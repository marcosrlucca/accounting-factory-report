//package io.sicredi.accounting.factory.report.business.service;
//
//import io.github.benas.randombeans.api.EnhancedRandom;
//import io.sicredi.accounting.factory.report.Application;
//import io.sicredi.accounting.factory.report.business.contabpdb.domain.Field;
//import io.sicredi.accounting.factory.report.business.contabpdb.service.FieldService;
//import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
//import io.sicredi.accounting.factory.report.business.pending.domain.enums.ParamQuery;
//import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
//import io.sicredi.accounting.factory.report.business.pending.repository.PendingRepository;
//import io.sicredi.accounting.factory.report.business.pending.service.PendingService;
//import io.sicredi.accounting.factory.report.infrastructure.exception.NotFoundException;
//import io.sicredi.accounting.factory.report.infrastructure.helper.FileHelper;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import reactor.core.publisher.Flux;
//import reactor.test.StepVerifier;
//
//import java.time.*;
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.doReturn;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes = Application.class)
//class PendingServiceTest {
//
//    private final static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2019, 01, 13, 13, 13, 13);
//
//    @MockBean
//    private FileHelper fileHelper;
//
//    @MockBean
//    private FieldService fieldService;
//
//    @MockBean
//    private PendingRepository pendingRepository;
//
//    @Autowired
//    private PendingService pendingService;
//
//    @Mock
//    private Clock clock;
//
//    //field that will contain the fixed clock
//    private Clock fixedClock;
//
//    private List<String> projection;
//    private List<String> filter;
//    private List<String> header;
//
//    private List<? super Object> row;
//
//    private PendingStatus status;
//
//    private String bucketName;
//
//    private List<Field> fields;
//
//    @BeforeEach
//    void setUp() {
//        projection = EnhancedRandom.randomListOf(10, String.class);
//        filter = EnhancedRandom.randomListOf(10, String.class);
//        header = EnhancedRandom.randomListOf(10, String.class);
//        row = EnhancedRandom.randomListOf(10, Object.class);
//        status = EnhancedRandom.random(PendingStatus.class);
//
//        fields = Arrays.asList(new Field(), new Field());
//
//        MockitoAnnotations.initMocks(this);
//
//        //tell your tests to return the specified LOCAL_DATE when calling LocalDate.now(clock)
//        Instant fixedInstant = LOCAL_DATE_TIME.atZone(ZoneId.systemDefault()).toInstant();
//        fixedClock = Clock.fixed(fixedInstant, ZoneId.systemDefault());
//        doReturn(fixedClock.instant()).when(clock).instant();
//        doReturn(fixedClock.getZone()).when(clock).getZone();
//
//        bucketName = "pending";
//
//        when(fieldService.findAll()).thenReturn(fields);
//    }
//
//    @Test
//    void makeReportTest_not_found() {
//        Map<ParamQuery, ? super Object> paramQuery = new HashMap<>();
//        paramQuery.put(ParamQuery.USER, "marcos");
//
//        var fileName = "marcos-" + LOCAL_DATE_TIME.toString();
//
//        Pending pending = new Pending();
//
//        when(pendingRepository.findPending(any(), any(), any())).thenReturn(Flux.just(pending));
//        when(fileHelper.generateFileName(any(), any())).thenReturn(fileName);
//        when(fileHelper.getUrl(any(), any())).thenReturn(Optional.empty());
//        doReturn(row).when(fileHelper).createRow(any(), any());
//        doNothing().when(fileHelper).write(projection, row, fileName, bucketName);
//
//        pendingService.setClock(fixedClock);
//
//        StepVerifier.create(pendingService.process(paramQuery))
//                .expectErrorMatches(ex -> ex instanceof NotFoundException)
//                .verify();
//    }
//
//    @Test
//    void makeReportTest() {
//        Map<ParamQuery, ? super Object> paramQuery = new HashMap<>();
//        paramQuery.put(ParamQuery.USER, "marcos");
//
//        var fileName = "marcos-" + LOCAL_DATE_TIME.toString();
//
//        Pending pending = new Pending();
//
//        when(pendingRepository.findPending(any(), any(), any())).thenReturn(Flux.just(pending));
//        when(fileHelper.generateFileName(any(), any())).thenReturn(fileName);
//        when(fileHelper.getUrl(any(), any())).thenReturn(Optional.of(fileName));
//        doReturn(row).when(fileHelper).createRow(any(), any());
//        doNothing().when(fileHelper).write(projection, row, fileName, bucketName);
//
//        pendingService.setClock(fixedClock);
//
//        StepVerifier.create(pendingService.process(paramQuery))
//                .assertNext(result -> assertEquals(fileName, result))
//                .verifyComplete();
//    }
//
//}
