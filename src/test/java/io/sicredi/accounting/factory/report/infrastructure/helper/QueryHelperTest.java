//package io.sicredi.accounting.factory.report.infrastructure.helper;
//
//import io.sicredi.accounting.factory.report.Application;
//import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
//import io.sicredi.accounting.factory.report.infrastructure.exception.BusinessException;
//import io.sicredi.accounting.factory.report.infrastructure.exception.ValidationException;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.time.format.DateTimeParseException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static java.util.Objects.requireNonNull;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
//@ExtendWith(SpringExtension.class)
//class QueryHelperTest {
//
//    private List<String> projection;
//
//    @BeforeEach
//    void setup() {
//        projection = new ArrayList<>();
//        projection.add("company");
//        projection.add("branch");
//        projection.add("date");
//        projection.add("account");
//        projection.add("value");
//        projection.add("type");
//    }
//
//    @Test
//    void createFiltersTest_pending_status_null() {
//        var filters = new ArrayList<String>();
//        filters.add("filter1:a");
//        filters.add("filter2:b");
//        filters.add("filter3:c");
//
//        Assertions.assertThrows(ValidationException.class, () -> QueryHelper.createFilters(filters, null));
//    }
//
//    @Test
//    void createFiltersTest_without_colon() {
//        var filters = new ArrayList<String>();
//        filters.add("filter1");
//        filters.add("filter2");
//        filters.add("filter3");
//
//        Assertions.assertThrows(ValidationException.class, () -> QueryHelper.createFilters(filters, null));
//    }
//
//    @Test
//    void createFiltersTest_start_value_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startValue:as");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(NumberFormatException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_end_value_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("endValue:as");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(NumberFormatException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_start_end_value_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startValue:null");
//        filters.add("endValue:null");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(NumberFormatException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_end_value_higher() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startValue:20");
//        filters.add("endValue:10");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(BusinessException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_start_date_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startDate:19/01/2019");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(DateTimeParseException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_end_date_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("endDate:19/01/2019");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(DateTimeParseException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest_start_end_date_invalid() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startDate:null");
//        filters.add("endDate:null");
//        filters.add("f2:a");
//        filters.add("f3:c");
//
//        Assertions.assertThrows(DateTimeParseException.class, () -> QueryHelper.createFilters(filters, status));
//    }
//
//    @Test
//    void createFiltersTest() {
//        var status = PendingStatus.OPEN;
//
//        var filters = new ArrayList<String>();
//        filters.add("startDate:2016-08-16T10:15:30+08:00");
//        filters.add("endDate:2017-08-16T10:15:30+08:00");
//        filters.add("startValue:2");
//        filters.add("endValue:3");
//        filters.add("group:a");
//        filters.add("f2:a");
//        filters.add("f3:c");
//        filters.add("justify:c");
//        filters.add("ticket:c");
//
//        var result = QueryHelper.createFilters(filters, status);
//
//        assertTrue(result.isPresent());
//
//        var criteria = result.get();
//
//        assertEquals(8, criteria.size());
//
//        var index = 0;
//
//        assertTrue(requireNonNull(criteria.get(index).getKey()).equalsIgnoreCase("pendingStatus"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("comment"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("comment"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("transaction.f2"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("transaction.f3"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("group"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("transaction.date"));
//        assertTrue(requireNonNull(criteria.get(++index).getKey()).equalsIgnoreCase("transaction.value"));
//    }
//
//    @Test
//    void createProjectionTest() {
//        var result = QueryHelper.createProjection(projection);
//
//        assertTrue(result.isPresent());
//
//        assertTrue(result.get().contains("transaction.company"));
//        assertTrue(result.get().contains("transaction.branch"));
//        assertTrue(result.get().contains("transaction.date"));
//        assertTrue(result.get().contains("transaction.account"));
//        assertTrue(result.get().contains("transaction.value"));
//        assertTrue(result.get().contains("transaction.type"));
//
//        result.get().forEach(inst -> assertTrue(inst.contains("transaction.")));
//    }
//
//}