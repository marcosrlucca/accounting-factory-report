package io.sicredi.accounting.factory.report.api.dto;

public class ErrorDTO {

    private int status;

    private String timestamp;
    private String path;
    private String error;
    private String message;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorDTO [timestamp=" + timestamp + ", path=" + path + ", status=" + status + ", error=" + error + ", message=" + message + "]";
    }

}