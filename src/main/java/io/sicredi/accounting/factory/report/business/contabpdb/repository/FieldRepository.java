package io.sicredi.accounting.factory.report.business.contabpdb.repository;

import io.sicredi.accounting.factory.report.business.contabpdb.domain.Field;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface FieldRepository extends JpaRepository<Field, BigDecimal> {
}
