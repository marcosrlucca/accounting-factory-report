package io.sicredi.accounting.factory.report.business.contabpdb.domain;

import io.sicredi.accounting.factory.report.infrastructure.entity.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "REC_FIELD_LABEL", schema = "CONTAB_OWNER")
public class Field extends AbstractEntity<BigDecimal> {

    private String description;
    private String field;

    @Id
    @Column(name = "FIELD_LABEL_ID")
    @Override
    public BigDecimal getId() {
        return super.getId();
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "FIELD")
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

}
