package io.sicredi.accounting.factory.report.business.pending.repository;

import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.time.ZonedDateTime;
import java.util.List;

public interface PendingRepositoryCustom {

    Logger LOGGER = LoggerFactory.getLogger(PendingRepositoryCustom.class);

    String PENDING_COLLECTION_NAME = "pendings";

    String TRANSACTION_NESTED_DOCUMENT_NAME = "transaction";

    @Transactional(propagation = Propagation.SUPPORTS)
    Flux<Pending> findPending(PendingStatus status, List<String> filters, List<String> projection, ZonedDateTime startDate, ZonedDateTime endDate);

}
