package io.sicredi.accounting.factory.report.business.pending.domain.enums;

public enum PendingStatus {

    OPEN, ARCHIVED;

    public static boolean contains(String value) {

        for (PendingStatus c : PendingStatus.values()) {
            if (c.name().equals(value)) {
                return true;
            }
        }

        return false;
    }

}
