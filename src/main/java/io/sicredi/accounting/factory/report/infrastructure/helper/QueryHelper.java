package io.sicredi.accounting.factory.report.infrastructure.helper;

import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import io.sicredi.accounting.factory.report.infrastructure.exception.BusinessException;
import io.sicredi.accounting.factory.report.infrastructure.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import reactor.core.Exceptions;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static io.sicredi.accounting.factory.report.business.pending.repository.PendingRepositoryCustom.TRANSACTION_NESTED_DOCUMENT_NAME;

public class QueryHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryHelper.class);

    private static final String DOT = ".";
    private static final String COLON = ":";
    private static final String COMMA = ",";
    private static final String PENDING_STATUS = "pendingStatus";
    private static final String START_VALUE = "startValue";
    private static final String END_VALUE = "endValue";
    private static final String GROUP = "group";
    private static final String TRANSACTION = "transaction";
    private static final String JUSTIFY = "justify";
    private static final String TICKET = "ticket";
    private static final String VALUE = "value";
    private static final String DATE = "date";
    private static final String COMMENT = "comment";
    private static final String YES = "yes";

    public static Optional<List<String>> createProjection(List<String> projection) {
        LOGGER.debug("Creating projection");

        final Optional<List<String>> maybeProjection = Optional.ofNullable(projection == null || projection.isEmpty() ? null : projection);

        return maybeProjection.map(a -> a.stream().map(b -> TRANSACTION_NESTED_DOCUMENT_NAME + DOT + b).collect(Collectors.toList()));
    }

    public static Optional<List<Criteria>> createFilters(List<String> filters, PendingStatus pendingStatus, ZonedDateTime startDate, ZonedDateTime endDate) {
        LOGGER.debug("Creating filters");

        if (filters.stream().anyMatch(f -> !f.contains(COLON)) || pendingStatus == null)
            throw new ValidationException("Filter is invalid");

        final Optional<Map<String, String>> maybeFilters = splitFilters(filters);

        final List<Criteria> criteria = new ArrayList<>();

        final Optional<ZonedDateTime> maybeStartDate = Optional.ofNullable(startDate);
        final Optional<ZonedDateTime> maybeEndDate = Optional.ofNullable(endDate);

        maybeStartDate
                .ifPresent(sd -> maybeEndDate
                        .ifPresentOrElse(ed -> criteria.add(Criteria.where(TRANSACTION + DOT + DATE).gte(sd).lte(ed)),
                                () -> criteria.add(Criteria.where(TRANSACTION + DOT + DATE).gte(sd))));

        maybeFilters
                .ifPresent(f -> {
                            criteria.add(Criteria.where(PENDING_STATUS).regex("^" + pendingStatus + "$", "i"));

                            for (String key : f.keySet()) {
                                if (key.equalsIgnoreCase(START_VALUE) || key.equalsIgnoreCase(END_VALUE))
                                    continue;

                                if (key.equalsIgnoreCase(GROUP)) {
                                    criteria.add(Criteria.where(GROUP).regex("^" + f.get(GROUP) + "$", "i"));
                                } else if (f.get(key).contains(COMMA)) {
                                    final List<String> listCriteria = Arrays.asList(f.get(key).split(COMMA));
                                    criteria.add(Criteria.where(TRANSACTION + DOT + key).in(listCriteria));
                                } else if (key.equalsIgnoreCase(JUSTIFY)) {
                                    createCriteriaForObservation(criteria, JUSTIFY, f.get(JUSTIFY));
                                } else if (key.equalsIgnoreCase(TICKET)) {
                                    createCriteriaForObservation(criteria, TICKET, f.get(TICKET));
                                } else {
                                    criteria.add(Criteria.where(TRANSACTION + DOT + key).regex("^" + f.get(key) + "$", "i"));
                                }
                            }

                            if (f.containsKey(START_VALUE) && f.containsKey(END_VALUE)) {
                                createCriteriaForValue(criteria, f.get(START_VALUE), f.get(END_VALUE));
                            } else if (f.containsKey(START_VALUE) && !f.containsKey(END_VALUE)) {
                                createCriteriaForValue(criteria, f.get(START_VALUE), null);
                            } else if (!f.containsKey(START_VALUE) && f.containsKey(END_VALUE)) {
                                createCriteriaForValue(criteria, null, f.get(END_VALUE));
                            }
                        }
                );

        return Optional.of(criteria);
    }

    private static Optional<Map<String, String>> splitFilters(List<String> filters) {
        LOGGER.debug("Splitting filters");

        final Optional<List<String>> maybeFilters = Optional.ofNullable(filters == null || filters.isEmpty() ? null : filters);

        //tamanho maximo do array é 2, porque, se nao, iria cortar os : que se encontram dentro da data
        return maybeFilters.map(f -> f.stream().map(a -> a.split(COLON, 2)).collect(Collectors.toMap(a -> a[0], a -> a[1])));
    }

    private static void createCriteriaForValue(List<Criteria> criteria, String startValue, String endValue) {
        LOGGER.debug("Creating criteria for values");

        final Optional<String> maybeStartValue = Optional.ofNullable(startValue);
        final Optional<String> maybeEndValue = Optional.ofNullable(endValue);

        try {
            if (maybeStartValue.isPresent() && maybeEndValue.isPresent()) {
                final Double startDouble = Double.parseDouble(startValue);
                final Double endDouble = Double.parseDouble(endValue);

                if (startDouble > endDouble) throw new BusinessException("Start value is higher than end value");

                criteria.add(Criteria.where(TRANSACTION + DOT + VALUE).gte(startDouble).lte(endDouble));
            } else if (maybeStartValue.isPresent() && maybeEndValue.isEmpty()) {
                final Double startDouble = Double.parseDouble(startValue);

                criteria.add(Criteria.where(TRANSACTION + DOT + VALUE).gte(startDouble));
            } else if (maybeStartValue.isEmpty() && maybeEndValue.isPresent()) {
                final Double endDouble = Double.parseDouble(endValue);

                criteria.add(Criteria.where(TRANSACTION + DOT + VALUE).lte(endDouble));
            }
        } catch (NumberFormatException e) {
            throw Exceptions.propagate(e);
        }
    }

    private static void createCriteriaForObservation(List<Criteria> criteria, String key, String value) {
        LOGGER.debug("Creating criteria for observation");

        if (!value.isEmpty()) {
            if (value.equalsIgnoreCase(YES)) {
                criteria.add(Criteria.where(COMMENT).elemMatch(Criteria.where(key).ne("")));
            } else {
                criteria.add(Criteria.where(COMMENT).not().elemMatch(Criteria.where(key).ne("")));
            }
        }
    }

}