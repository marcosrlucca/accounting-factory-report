package io.sicredi.accounting.factory.report.business.contabpdb.service;

import io.sicredi.accounting.factory.report.business.contabpdb.domain.Field;
import io.sicredi.accounting.factory.report.business.contabpdb.repository.FieldRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FieldService {

    public Logger LOGGER = LoggerFactory.getLogger(FieldService.class);

    private final FieldRepository repository;

    @Autowired
    public FieldService(FieldRepository repository) {
        this.repository = repository;
    }

    public Collection<Field> findAll() {
        LOGGER.debug("finding all fieds for pending");

        return repository.findAll();
    }

}
