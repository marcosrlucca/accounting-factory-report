package io.sicredi.accounting.factory.report.infrastructure.exception;

public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException() {
        super();
    }

}
