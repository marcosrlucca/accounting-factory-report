package io.sicredi.accounting.factory.report.infrastructure.exception;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException() {}

}
