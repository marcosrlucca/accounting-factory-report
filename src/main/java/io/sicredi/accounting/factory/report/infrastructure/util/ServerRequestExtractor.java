package io.sicredi.accounting.factory.report.infrastructure.util;

import io.sicredi.accounting.factory.report.business.pending.domain.enums.ParamQuery;
import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static io.sicredi.accounting.factory.report.business.pending.domain.enums.ParamQuery.*;

public class ServerRequestExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerRequestExtractor.class);

    private static final String SPLIT_DELIMITER = ";";

    public static Map<ParamQuery, ? super Object> extractParamQuery(String projection, String filter, String status, String user, String startDate, String endDate) {
        final Map<ParamQuery, ? super Object> param = new HashMap<>();

        extractProjectionRequest(projection).ifPresent(p -> param.put(PROJECTION, p));
        extractFilterRequest(filter).ifPresent(p -> param.put(DYNAMIC_FILTER, p));
        extractStatusRequest(status).ifPresent(p -> param.put(STATUS, p));
        extractUser(user).ifPresent(p -> param.put(USER, p));
        extractDate(startDate).ifPresent(p -> param.put(START_DATE, p));
        extractDate(endDate).ifPresent(p -> param.put(END_DATE, p));

        return param;
    }

    public static List<String> getListParamQueryOrDefault(Map<ParamQuery, ? super Object> paramQuery, ParamQuery projection) {
        LOGGER.debug("Extracting list param query");

        return ((List<?>) paramQuery.getOrDefault(projection, new ArrayList<>()))
                .stream()
                .filter(a -> a instanceof String)
                .map(a -> (String) a)
                .collect(Collectors.toList());
    }

    private static Optional<List<String>> extractProjectionRequest(String projection) {
        LOGGER.debug("Extracting projection");

        return Optional.ofNullable(isEmpty(projection)).map(a -> Arrays.asList(a.split(SPLIT_DELIMITER)));
    }

    private static Optional<List<String>> extractFilterRequest(String filter) {
        LOGGER.debug("Extracting filter");

        return Optional.ofNullable(isEmpty(filter)).map(a -> Arrays.asList(a.split(SPLIT_DELIMITER)));
    }

    private static Optional<PendingStatus> extractStatusRequest(String status) {
        LOGGER.debug("Extracting status");

        return Optional.ofNullable(isEmpty(status)).filter(PendingStatus::contains).map(PendingStatus::valueOf);
    }

    private static Optional<String> extractUser(String user) {
        LOGGER.debug("Extracting user");

        return Optional.ofNullable(isEmpty(user));
    }

    private static Optional<ZonedDateTime> extractDate(String date) {
        LOGGER.debug("Extracting date");

        return Optional.ofNullable(isEmpty(date)).map(ZonedDateTime::parse);
    }

    private static String isEmpty(String value) {
        return StringUtils.isEmpty(value) ? null : value;
    }

}