package io.sicredi.accounting.factory.report.infrastructure.exception;

public class S3Exception extends Exception {

    private static final long serialVersionUID = 5986472213681944807L;

    public S3Exception() {
        super();
    }

    public S3Exception(Throwable t) {
        super(t);
    }
}
