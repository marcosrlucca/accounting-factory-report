package io.sicredi.accounting.factory.report.infrastructure.entity;

import org.springframework.data.annotation.Id;
import java.io.Serializable;

public abstract class AbstractEntity<T extends Serializable & Comparable<T>> implements Serializable {

    @Id
    private T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
