package io.sicredi.accounting.factory.report.infrastructure.aws;

public enum S3Folder {
    IN("in/"), OUT("out/");

    private final String value;

    private S3Folder(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
