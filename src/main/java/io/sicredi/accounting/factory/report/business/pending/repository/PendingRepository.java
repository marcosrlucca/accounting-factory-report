package io.sicredi.accounting.factory.report.business.pending.repository;

import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendingRepository extends ReactiveMongoRepository<Pending, String>, PendingRepositoryCustom {
}
