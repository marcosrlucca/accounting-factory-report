package io.sicredi.accounting.factory.report.infrastructure.helper;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
import io.sicredi.accounting.factory.report.infrastructure.aws.S3Client;
import io.sicredi.accounting.factory.report.infrastructure.exception.BusinessException;
import io.sicredi.accounting.factory.report.infrastructure.exception.S3Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.Exceptions;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class FileHelper {

    private Logger LOGGER = LoggerFactory.getLogger(FileHelper.class);

    public static final String FILE_NAME_SEPARATOR = "-";
    public static final String DIRECTORY_SEPARATOR = "/";
    public static final String EMPTY = "";

    @Value("${report.export.file.extension}")
    private String fileExtension;

    @Value("${application.aws.s3.bucket-name}")
    private String bucketName;

    private final S3Client s3Client;

    @Autowired
    public FileHelper(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public List<? super Object> createRow(List<String> header, Pending value) {
        LOGGER.debug("Creating row");

        if (value.getTransaction() == null || value.getTransaction().isEmpty())
            throw new BusinessException("Pending transaction is empty");

        final var row = new ArrayList<>();

        for (String key : header) {
            var v = value.getTransaction().getOrDefault(key, EMPTY);

            if (v instanceof Date)
                row.add(((Date) v).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.ofPattern("d/M/y")));
            else
                row.add(v);
        }

        return row;
    }

    public String generateFileName(String providerName, LocalDateTime generationDate) {
        return providerName + FILE_NAME_SEPARATOR + generationDate + fileExtension;
    }

    public void write(List<String> projection, List<?> row, String fileName, String directory) {
        if (!Files.exists(Paths.get(getPathName(fileName)))) {
            LOGGER.debug("Writing file header");

            write(projection, fileName, directory); //write first the header
            write(row, fileName, directory); //then, write the first line of the table body
        } else {
            LOGGER.debug("Writing file {}", fileName);

            write(row, fileName, directory); //write the row into the body
        }
    }

    public void write(List<?> row, String fileName, String directory) {
        final File file = new File(getPathName(fileName));

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8))) {
            for (Object aRow : row) {
                bw.write(String.valueOf(aRow).concat(";"));
            }

            bw.newLine();
            bw.flush();

            saveFileToS3(fileName, file, directory);
        } catch (S3Exception | IOException e) {
            LOGGER.error(e.getLocalizedMessage());

            throw Exceptions.propagate(e);
        }
    }

    public Optional<String> getUrl(String directory, String fileName) {
        return s3Client.getUrl(directory, fileName, bucketName);
    }

    private String getPathName(String fileName) {
        return System.getProperty("java.io.tmpdir") + DIRECTORY_SEPARATOR + fileName;
    }

    private void saveFileToS3(String fileName, File file, String directory) throws S3Exception {
        s3Client.uploadFile(fileName, bucketName, file, directory, CannedAccessControlList.PublicRead);
    }

}
