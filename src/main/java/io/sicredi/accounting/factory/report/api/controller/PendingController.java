package io.sicredi.accounting.factory.report.api.controller;

import io.sicredi.accounting.factory.report.business.pending.service.PendingService;
import io.sicredi.accounting.factory.report.infrastructure.util.ServerRequestExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import java.net.URI;

@RestController
@RequestMapping("/pending")
public class PendingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PendingController.class);

    private final PendingService service;

    @Autowired
    public PendingController(PendingService service) {
        this.service = service;
    }

    @GetMapping
    public Mono<ResponseEntity> makeReport(@RequestParam(value = "user", required = false) String user,
                                           @RequestParam(value = "status", required = false) String status,
                                           @RequestParam(value = "projection") String projection,
                                           @RequestParam(value = "dynamicFilter", required = false) String dynamicFilter,
                                           @RequestParam(value = "startDate") String startDate,
                                           @RequestParam(value = "endDate", required = false) String endDate) {
        LOGGER.debug("Starting report construction...");

        ParamQueryValidator.validate(projection, dynamicFilter, status, user, startDate, endDate);

        LOGGER.debug("Validated");

        final var paramQuery = ServerRequestExtractor.extractParamQuery(projection, dynamicFilter, status, user, startDate, endDate);

        LOGGER.debug("Param query extracted");

        return service.process(paramQuery)
                .map(result -> {
                    if (result.isEmpty())
                        return ResponseEntity.noContent().build();

                    return ResponseEntity.created(URI.create(result)).build();
                });
    }

}