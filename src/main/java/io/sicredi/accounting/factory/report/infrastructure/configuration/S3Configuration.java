package io.sicredi.accounting.factory.report.infrastructure.configuration;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import io.sicredi.services.platform.aws.RemoteAwsCredentialsProviderChain;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Configuration {

    @Bean
    public AmazonS3 s3ClientConfiguration(final RemoteAwsCredentialsProviderChain providerChain, @Value("${application.aws.region}") final String region) {
        return AmazonS3ClientBuilder.standard().withCredentials(providerChain).withRegion(region).build();
    }

}