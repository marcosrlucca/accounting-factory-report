package io.sicredi.accounting.factory.report.business.pending.service;

import io.sicredi.accounting.factory.report.business.contabpdb.domain.Field;
import io.sicredi.accounting.factory.report.business.contabpdb.service.FieldService;
import io.sicredi.accounting.factory.report.business.pending.repository.PendingRepository;
import io.sicredi.accounting.factory.report.business.pending.domain.enums.ParamQuery;
import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import io.sicredi.accounting.factory.report.infrastructure.exception.NotFoundException;
import io.sicredi.accounting.factory.report.infrastructure.helper.FileHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.sicredi.accounting.factory.report.business.pending.domain.enums.ParamQuery.*;
import static io.sicredi.accounting.factory.report.infrastructure.helper.FileHelper.EMPTY;
import static io.sicredi.accounting.factory.report.infrastructure.helper.FileHelper.FILE_NAME_SEPARATOR;
import static io.sicredi.accounting.factory.report.infrastructure.util.ServerRequestExtractor.getListParamQueryOrDefault;

@Service
public class PendingService {

    private Logger LOGGER = LoggerFactory.getLogger(PendingService.class);

    @Value("${report.export.file.name}")
    private String providerName;

    @Value("${report.export.directory.pending}")
    private String directory;

    private final PendingRepository pendingRepository;

    private final FileHelper fileHelper;

    private final FieldService fieldService;

    private Clock clock = Clock.systemDefaultZone();

    private Collection<Field> labels;

    @Autowired
    public PendingService(PendingRepository pendingRepository, FileHelper fileHelper, FieldService fieldService) {
        this.pendingRepository = pendingRepository;
        this.fileHelper = fileHelper;
        this.fieldService = fieldService;

        labels = fieldService.findAll();
    }

    public Mono<String> process(Map<ParamQuery, ? super Object> paramQuery) {
        LOGGER.debug("Making report");

        final List<String> projection = getListParamQueryOrDefault(paramQuery, PROJECTION);
        final List<String> filter = getListParamQueryOrDefault(paramQuery, DYNAMIC_FILTER);
        final ZonedDateTime startDate = (ZonedDateTime) paramQuery.get(START_DATE);
        final ZonedDateTime endDate = (ZonedDateTime) paramQuery.get(END_DATE);
        final List<String> mappedProjection = mapProjection(projection).orElse(projection);
        final PendingStatus status = (PendingStatus) paramQuery.getOrDefault(STATUS, PendingStatus.OPEN);

        final String fileName = Optional.ofNullable((String) paramQuery.get(USER))
                .map(u -> u.concat(FILE_NAME_SEPARATOR).concat(fileHelper.generateFileName(providerName, LocalDateTime.now(clock))))
                .orElse(fileHelper.generateFileName(providerName, LocalDateTime.now(clock)));

        return pendingRepository.findPending(status, filter, projection, startDate, endDate)
                .map(r -> fileHelper.createRow(projection, r))
                .doOnNext(row -> fileHelper.write(mappedProjection, row, fileName, directory))
                .switchIfEmpty(Mono.empty())
                .flatMap(a -> fileHelper.getUrl(directory, fileName)
                        .map(Flux::just)
                        .orElse(Flux.error(new NotFoundException())))
                .distinct()
                .reduce(EMPTY, (a1, url) -> url);
    }

    private Optional<List<String>> mapProjection(List<String> projection) {
        var maybeProjection = Optional.ofNullable(projection);
        var maybeLabels = Optional.ofNullable(labels).map(lab -> lab.stream().collect(Collectors.toMap(Field::getField, Field::getDescription)));

        return maybeProjection
                .flatMap(projList -> maybeLabels
                        .map(labelMap -> projList.stream().map(projInst -> labelMap.getOrDefault(projInst, projInst)).collect(Collectors.toList())));
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }
}
