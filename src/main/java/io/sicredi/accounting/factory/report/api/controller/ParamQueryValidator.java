package io.sicredi.accounting.factory.report.api.controller;

import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import io.sicredi.accounting.factory.report.infrastructure.exception.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.regex.Pattern;

class ParamQueryValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamQueryValidator.class);

    private ParamQueryValidator() {
    }

    static void validate(String projection, String filter, String status, String user, String startDate, String endDate) {
        LOGGER.debug("Validating projection, filter, status and user");

        final var filterRegex = "[a-zA-Z0-9:;,-]*";
        final var projectionRegex = "[a-zA-Z0-9;]*";

        if (!StringUtils.isBlank(projection) && !Pattern.matches(projectionRegex, projection)) {
            LOGGER.debug("Projection invalid");

            throw new ValidationException("Projection is invalid");
        }

        if (filter != null && (StringUtils.containsWhitespace(filter.trim()) || !Pattern.matches(filterRegex, filter))) {
            LOGGER.debug("Filter invalid");

            throw new ValidationException("Filter is invalid");
        }

        if (!StringUtils.isBlank(status) && !PendingStatus.contains(status)) {
            LOGGER.debug("Status invalid");

            throw new ValidationException("Status is invalid");
        }

        if (user != null && (user.isEmpty() || StringUtils.containsWhitespace(user))) {
            LOGGER.debug("User invalid");

            throw new ValidationException("User is invalid");
        }

        validateDatePattern(startDate);
        validateDatePattern(endDate);
    }

    private static void validateDatePattern(String date) {
        var maybeDate = Optional.ofNullable(date);

        maybeDate.ifPresent(d -> {
            if (StringUtils.containsWhitespace(d)) {
                LOGGER.debug("Date contains white space");

                throw new ValidationException("Date contains white space");
            }

            try {
                ZonedDateTime.parse(d);
            } catch (DateTimeParseException ex) {
                LOGGER.debug("Date is invalid");

                throw new ValidationException("Date is invalid");
            }
        });
    }

}
