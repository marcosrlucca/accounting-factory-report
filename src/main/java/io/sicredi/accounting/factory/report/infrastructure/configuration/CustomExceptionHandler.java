package io.sicredi.accounting.factory.report.infrastructure.configuration;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import javax.naming.ServiceUnavailableException;

import io.sicredi.accounting.factory.report.api.dto.ErrorDTO;
import io.sicredi.accounting.factory.report.infrastructure.exception.*;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ServerWebInputException;
import com.fasterxml.jackson.core.JsonProcessingException;

@ControllerAdvice
public class CustomExceptionHandler {

    private final MessageSource messageSource;

    public CustomExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(S3Exception.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(S3Exception exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(NotFoundException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(NumberFormatException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(DateTimeParseException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(ValidationException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ErrorDTO> businessExceptionHandler(BusinessException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ErrorDTO> unauthorizedExceptionHandler(UnauthorizedException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ErrorDTO> serviceUnavailableExceptionHandler(ServiceUnavailableException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ErrorDTO> validationHandler(WebExchangeBindException exception, ServerHttpRequest request) throws JsonProcessingException {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, getFieldErrorMessage(Objects.requireNonNull(exception.getFieldError())), request.getURI().getRawPath()));
    }

    @ExceptionHandler(WebClientResponseException.class)
    public ResponseEntity<ErrorDTO> webClientResponseExceptionHandler(WebClientResponseException exception, ServerHttpRequest request) {
        HttpStatus status = exception.getStatusCode();

        return ResponseEntity.status(status).body(generateErrorDTO(status, exception.getMessage(), request.getURI().getRawPath()));
    }

    @ExceptionHandler(ServerWebInputException.class)
    public ResponseEntity<ErrorDTO> throwableHandler(ServerWebInputException exception, ServerHttpRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).body(generateErrorDTO(status, NestedExceptionUtils.getMostSpecificCause(exception).getMessage(), request.getURI().getRawPath()));
    }

    private ErrorDTO generateErrorDTO(HttpStatus status, String message, String path) {
        ErrorDTO dto = new ErrorDTO();

        dto.setError(status.getReasonPhrase());
        dto.setStatus(status.value());
        dto.setMessage(message);
        dto.setTimestamp(ZonedDateTime.now().toString());
        dto.setPath(path);

        return dto;
    }

    private String getFieldErrorMessage(FieldError fieldError) {
        try {
            return this.messageSource.getMessage(fieldError.getDefaultMessage(), fieldError.getArguments(), LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            return fieldError.getDefaultMessage();
        }
    }

}
