package io.sicredi.accounting.factory.report.business.pending.domain;

import io.sicredi.accounting.factory.report.infrastructure.entity.AbstractEntity;

import java.util.Map;

public class Pending extends AbstractEntity<String> {

    private Map<String, ? super Object> transaction;

    public Map<String, ? super Object> getTransaction() {
        return transaction;
    }

    public void setTransaction(Map<String, ? super Object> transaction) {
        this.transaction = transaction;
    }

}