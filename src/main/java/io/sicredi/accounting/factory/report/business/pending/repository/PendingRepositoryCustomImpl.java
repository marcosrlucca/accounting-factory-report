package io.sicredi.accounting.factory.report.business.pending.repository;

import io.sicredi.accounting.factory.report.business.pending.domain.Pending;
import io.sicredi.accounting.factory.report.business.pending.domain.enums.PendingStatus;
import io.sicredi.accounting.factory.report.infrastructure.helper.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public class PendingRepositoryCustomImpl implements PendingRepositoryCustom {

    private final ReactiveMongoTemplate mongoTemplate;

    @Autowired
    public PendingRepositoryCustomImpl(ReactiveMongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Flux<Pending> findPending(PendingStatus status, List<String> filters, List<String> projection, ZonedDateTime startDate, ZonedDateTime endDate) {
        LOGGER.debug("Finding pendings on data base");

        final Query query = new Query();

        final Optional<List<Criteria>> maybeCriteria = QueryHelper.createFilters(filters, status, startDate, endDate);

        final Optional<List<String>> maybeProjection = QueryHelper.createProjection(projection);

        maybeProjection.ifPresent(a -> a.forEach(b -> query.fields().include(b)));

        maybeCriteria.ifPresent(c -> query.addCriteria(new Criteria().andOperator(c.toArray(new Criteria[c.size()]))));

        return mongoTemplate.find(query, Pending.class, PENDING_COLLECTION_NAME);
    }

}