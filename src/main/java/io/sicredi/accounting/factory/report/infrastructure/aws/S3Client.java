package io.sicredi.accounting.factory.report.infrastructure.aws;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import io.sicredi.accounting.factory.report.infrastructure.exception.S3Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.util.Optional;

@Component
public class S3Client implements Serializable {

    private static final long serialVersionUID = -6747807741674453351L;
    private static Logger logger = LoggerFactory.getLogger(S3Client.class.getSimpleName());

    private final AmazonS3 amazonS3;

    public S3Client(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public void uploadFile(String fileName, String bucketName, File file, String directory, CannedAccessControlList accessControl) throws S3Exception {
        try {
            this.amazonS3.putObject(new PutObjectRequest(bucketName, getKey(fileName, directory), file).withCannedAcl(accessControl));
            logger.debug("===================== Upload File - Done! =====================");
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException from PUT requests, rejected reasons:");
            logger.error(String.format("Error Message:     %s", ase.getMessage()));
            logger.error(String.format("HTTP Status Code:  %s", ase.getStatusCode()));
            logger.error(String.format("AWS Error Code:    %s", ase.getErrorCode()));
            logger.error(String.format("Error Type:        %s", ase.getErrorType()));
            logger.error(String.format("Request ID:        %s", ase.getRequestId()));

            throw new S3Exception(ase);
        } catch (AmazonClientException ace) {
            logger.error("Caught an AmazonClientException: ");
            logger.error(String.format("Error Message:  %s", ace.getMessage()));

            throw new S3Exception(ace);
        }
    }

    private String getKey(String fileName, String directory) {
        return S3Folder.OUT.getValue() + directory + fileName;
    }

    public Optional<String> getUrl(String directory, String fileName, String bucketName) {
        return Optional.ofNullable(this.amazonS3.getUrl(bucketName, getKey(fileName, directory))).map(URL::toString);
    }
}
