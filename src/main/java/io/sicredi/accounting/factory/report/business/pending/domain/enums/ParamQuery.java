package io.sicredi.accounting.factory.report.business.pending.domain.enums;

public enum ParamQuery {

    STATUS("status"),
    DYNAMIC_FILTER("dynamicFilter"),
    PROJECTION("projection"),
    USER("user"),
    START_DATE("startDate"),
    END_DATE("endDate");

    private String value;

    ParamQuery(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
